class Post < ActiveRecord::Base
  has_many :comments, dependent: :destroy #when we delete a post, it's going to delete all of the comments that depent on that post.
  validates_presence_of :title
  validates_presence_of :body
end
